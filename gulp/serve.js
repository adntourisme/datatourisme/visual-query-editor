/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');

function serve() {
    var options = {
        open: false,
        server: {
            baseDir: [
                '.tmp',
                'app'
            ]
        },
        files: [
            '.tmp/styles/**/*.css',
            '.tmp/scripts/**/*.js',
            'app/*.html'
        ]
    };
    browserSync(options);
}

module.exports = {
    dev: function() {
        serve();
    },
    dist: function() {
        console.log("todo")
    }
};