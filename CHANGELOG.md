CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [En cours]

----------------

## [1.2.3] - 2020.06.12

### Correction
- Bugfix : fix bool input translation

----------------

## [1.2.2] - 2020.01.30

### Correction
- Bugfix : remove thesaurus input functional limitation

----------------

## [1.2.1] - 2020.01.28

### Correction
- Period-filter now use xsd:dateTime instead of xsd:datetime

----------------

## [1.2.0] - 2019.01.14

### Modification
- Modification de l'IRI des ontologies DATAtourisme

----------------

## [1.1.0] - 2018.06.18

### Ajout
- Ajout du mécanisme de traduction
- Ajout de la traduction en anglais

----------------

## [1.0.2] - 2018.04.03

### Modification
- Modification de l'icone des Produits

----------------

## [1.0.1] - 2018.03.28

### Modification
- Ajout du code insee de la commune dans le filtre thesaurus

----------------

## [1.0.0] - 2018.03.01

### Ajouts
- Première release
- Filtres type, time

### Corrections
- ?sPointOfInterest1 -> ?res
- ontology.js dependance