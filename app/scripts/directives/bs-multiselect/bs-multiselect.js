/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('bootstrap-multiselect');

require('angular')
    .module('visual-query-editor')
    .directive('bsMultiselect', bsMultiselectDirective);

var $ = require('jquery');

/**
 * @ngInject
 */
function bsMultiselectDirective($timeout) {
    return {
        restrict: "A",
        require: "ngModel",
        link: link
    };

    /**
     * link
     */
    function link(scope, elem, attrs, ctrl) {
        $timeout(function() {
            $(elem).multiselect({
                onChange: function(element, checked) {
                    // fix library bug
                    if(element instanceof Array) {
                        this.$select.change();
                    }
                },
                allSelectedText: 'Tous',
                nonSelectedText: 'Aucun',
                nSelectedText: ' sélectionné(s)',
                inheritClass: true,
                enableHTML: true
            });
            // fix library behavior
            $("li.multiselect-group", $(elem).parent()).each(function(index, item) {
                $(item).addClass("multiselect-group-" + index);
                $("label", item).addClass("checkbox");
            });
        });
    }
}

/**
 * Fix internal multiselect mergeOptions to avoid bug with angular 1.6
 *
 * @param options
 * @returns {void|*}
 */
$.fn.multiselect.Constructor.prototype.mergeOptions = function(options) {
    for(var name in options) {
        if(name.indexOf('$') == 0) {
            delete options[name];
        }
    }
    return $.extend(true, {}, this.defaults, this.options, options);
};

