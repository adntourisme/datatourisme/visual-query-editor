/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .directive('emptyToNull', emptyToNullDirective);

/**
 * @ngInject
 */
function emptyToNullDirective() {
    return {
        restrict: "A",
        require: "ngModel",
        link: link
    };

    /**
     * link
     */
    function link(scope, elem, attrs, ctrl) {
        ctrl.$parsers.push(function(viewValue) {
            if(!viewValue || viewValue === "") {
                return null;
            }
            return viewValue;
        });
    }
}
