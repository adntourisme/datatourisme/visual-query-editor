/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

window.jQuery = window.$ = require('jquery');
window.Selectize = require('selectize');
window.moment = require('moment');

window.CodeMirror = require('codemirror');
require('codemirror/mode/sparql/sparql');
require('codemirror-graphql/mode');

var angular = require('angular');
require('angular-i18n/angular-locale_fr');

require('bootstrap');
require('leaflet');
require('ui-leaflet');
require('angular-simple-logger');
require('bootstrap-slider');
require('angular-bootstrap-slider');
require('nanoscroller');
require('./vendors/scrollable.js');
require('selectize');
require('angular-selectize2/dist/selectize');
require('angular-ui-codemirror');


require('bootstrap-daterangepicker');
require('angular-daterangepicker');

require('masonry-layout');
require('imagesloaded');

require('./vendors/*', {mode: 'expand'});

// module definition
var app = angular.module('visual-query-editor', [
    'nemLogging',
    'ui-leaflet',
    'ui.bootstrap-slider',
    'sun.scrollable',
    'selectize',
    'daterangepicker',
    'ui.codemirror',
    require('angular-ui-mask'),
    require('angular-masonry'),
    require('ng-dialog'),
    require('angular-translate')
]);
app.config(config);

// components
require('./components/*/*.js', {mode: 'expand'});
require('./directives/*/*.js', {mode: 'expand'});
require('./filters/*/*.js', {mode: 'expand'});
require('./services/*/*.js', {mode: 'expand'});

// Replace/Create the global namespace
window.Ontology = require('ontology-js');

/**
 * @ngInject
 */
function config($qProvider, $translateProvider) {
    $qProvider.errorOnUnhandledRejections(false);
    $translateProvider
        .translations('fr', require("../translations/fr.yml"))
        .translations('en', require("../translations/en.yml"))
        .fallbackLanguage('fr')
        .preferredLanguage('en');
}