/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .component('resultsMap', {
        template: require("./results-map.html"),
        controller: controller,
        bindings: {
            items: '<'
        }
    });

/**
 * @ngInject
 */
function controller($filter, leafletData) {
    var $ctrl = this;
    this.center = {
        lat: 46.7144,
        lng: 2.8729,
        zoom: 6
    };

    this.markers = [];

    this.$onChanges = function(bindings) {
        this.markers = [];
        for(var i=0; i<this.items.length; i++) {
            var label = $filter("path")(this.items[i], "rdfs_label.value");
            var lat = $filter("path")(this.items[i], "isLocatedAt.schema_geo.schema_latitude");
            var lng = $filter("path")(this.items[i], "isLocatedAt.schema_geo.schema_longitude");
            if(lat && lng) {
                this.markers.push({
                    lat: lat,
                    lng: lng,
                    message: "<a href='" + this.items[i]._uri + "' target='_blank'>" + label + "</a>"
                })
            }
        }

        leafletData.getMap("results-map").then(function(map) {
            map.fitBounds($ctrl.markers, {maxZoom: 8});
        });
    };

/*
    this.markers = [{
            lat: 59.91,
            lng: 10.75,
            message: "I want to travel here!",
            focus: false,
            draggable: false
        }
    ];

    var bounds = [];
    for (var i = 0; i < this.markers.length; i++) {
        bounds.push([this.markers[i].lat, this.markers[i].lng]);
    }
    leafletData.getMap().then(function(map) {
        map.fitBounds(bounds);
    });*/
}