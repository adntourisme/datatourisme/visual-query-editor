/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var Query = require('graphql-query-builder');

require('angular')
    .module('visual-query-editor')
    .component('resultItem', {
        template: require("./result-item.html"),
        controller: controller,
        bindings: {
            item: '<'
        }
    });

/**
 * @ngInject
 */
function controller($filter) {

    /**
     * Return icon according to the POI type
     *
     * @returns {*}
     */
    this.getIcon = function() {
        var icons = {
            "EntertainmentAndEvent": "calendar",
            "PlaceOfInterest": "flag",
            "Product": "euro",
            "Tour": "map-signs"
        };
        for(var i=0; i< this.item.rdf_type.length; i++) {
            var parts = this.item.rdf_type[i].split(/[\/:#]/);
            if(icons[parts[parts.length - 1]]) {
                return icons[parts[parts.length - 1]];
            }
        }
        return "map-marker";
    };

    /**
     *
     * @param date
     * @returns {*}
     */
    this.formatDate = function(date) {
        if(date) {
            var date = new Date(date);
            return $filter('date')(date, 'dd/MM/yyyy');
        }
    };
}