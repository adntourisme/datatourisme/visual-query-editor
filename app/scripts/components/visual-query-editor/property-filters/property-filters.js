/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var $ = require("jquery");
require('angular')
    .module('visual-query-editor')
    .component('propertyFilters', {
        template: require("./property-filters.html"),
        controller: controller,
        require: {
            ngModelCtrl: 'ngModel'
        },
        bindings: {
            ngModel: "<",
            types: "<"
        }
    });

require('./property-filter-input');

/**
 * @ngInject
 */
function controller($scope, dialogService, ontology, $timeout) {
    var $ctrl = this;
    this.model = {"or": [{"and": []}]};

    /**
     * $onChanges
     */
    this.$onChanges = function(bindings) {
        if(bindings.ngModel && bindings.ngModel.isFirstChange() && bindings.ngModel.currentValue) {
            this.model = angular.copy(bindings.ngModel.currentValue);
        }
    };

    // watch deeply filters to update ng-model
    $scope.$watch(function() { return $ctrl.model }, function(value) {
        var filtered = filterEmptyValues(angular.copy(value));
        $ctrl.ngModelCtrl.$setViewValue(filtered);
    }, true);


    /**
     * Remove empty definition (value == null)
     *
     * @param filter
     * @returns {*}
     */
    function filterEmptyValues(filter) {
        var key = Object.keys(filter)[0];
        if(Array.isArray(filter[key])) {
            filter[key] = filter[key].filter(function(child) {
                return filterEmptyValues(child) != null;
            });
            return filter[key].length > 0 ? filter : null;
        } else {
            var value = filter[key].value;
            if(value == undefined || value == null) {
                return null;
            }
            return filter;
        }
    }

    /**
     * Load a filter definition
     *
     * @param filter
     */
    this.loadFilter = function(filter) {

        var key = Object.keys(filter)[0];
        var params = filter[key];

        var path = angular.copy(params.path);
        var properties = [];
        var property = false;
        var ranges = [ontology.getClass(ontology.NS + "PointOfInterest")];
        while(path.length > 0) {
            var part = path.shift();
            property = false;
            loop: for(var i=0; i<ranges.length; i++) {
                var candidateProps = ranges[i].getCandidateProperties();
                for(var j=0; j<candidateProps.length; j++) {
                    if(candidateProps[j].hasUri(part)) {
                        property = candidateProps[j];
                        properties.push(property);
                        ranges = property.getRanges();
                        break loop;
                    }
                }
            }

            if(!property) {
                this.removeFilter(filter);
                return null;
            }
        }

        return {
            type: key,
            path: properties.map(function(p) { return p.getPreferedLabel() }).join(" > "),
            property: property,
            params: params
        }
    };

    /**
     * Add a filter
     */
    this.addFilter = function(group) {
        var dialog = dialogService.open({
            template: require('./add-filter-modal.html'),
            fullHeight: true
        });

        dialog.then(function (selection) {

            var obj = {};
            obj[selection.type] = {
                path: selection.path,
                value: null
            };
            group.and.push(obj);

            $timeout(function() {
                // handle autofocus
                var autofocus = $(".box-filters .form-group:last [autofocus]");
                if(autofocus.length) {
                    autofocus.focus();
                } else {
                    $(".box-filters .form-group:last :input:first").focus();
                }
                // handle autoclick
                var autoclick = $(".box-filters .form-group:last [autoclick]");
                autoclick.click();
            });

        }, function() {});
    };
    //this.addFilter();

    /**
     * Remove a filter
     *
     * @param filter
     */
    this.removeFilter = function(filter) {
        for(var i=0; i<$ctrl.model.or.length; i++) {
            var group = $ctrl.model.or[i].and;
            var pos = group.indexOf(filter);
            if(pos > -1) {
                group.splice(pos, 1);
            }
        }
    };

}
