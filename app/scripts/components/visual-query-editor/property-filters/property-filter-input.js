/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .component('propertyFilterInput', {
        controller: controller,
        //template: template,
        bindings: {
            property: '<',
            type: '<',
            ngModel: '=',
            types:'<'
        }
    });

/**
 * @ngInject
 */
function controller($element, $scope, $compile, filtersRepository) {
    this.$onChanges = function (obj) {
        var property = obj.property.currentValue;
        var filter = filtersRepository.resolve(property.getUri(), this.type);
        if(filter) {
            $element.append($compile(filter.template())($scope));
        }
    };
}