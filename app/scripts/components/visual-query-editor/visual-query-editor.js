/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var Query = require('graphql-query-builder');
var SparqlParser = require('sparqljs').Parser;
var SparqlGenerator = require('sparqljs').Generator;

require('angular')
    .module('visual-query-editor')
    .component('visualQueryEditor', {
        template: require("./visual-query-editor.html"),
        controller: controller,
        require: {
            ngModelCtrl: 'ngModel'
        },
        bindings: {
            ngModel: "<",
            query: '=?',
            mode: "@"
        }
    });

require('./property-filters/property-filters');
require('./result-item/result-item');
require('./results-map/results-map');

/**
 * @ngInject
 */
function controller($scope, ontology, $timeout, filtersRepository, graphqlClient, dialogService) {
    var $ctrl = this;

    // type options for type selection
    this.typeOptions = getTypeOptions(ontology.getClass(ontology.NS + "PointOfInterest"));

    // special object to act as proxy between form and ngModel
    this.proxyTypesValues = [];

    // boolean loading
    this.loading = false;

    // last apollo error
    this.error = null;

    // graphQL object
    this.graphQL = null;

    // generated query
    this.query = null;

    // resultSet object
    this.resultSet = null;

    this.model = {
        types:[],
        ast: {"or": [{"and": []}]}
    };

    // resultSet object
    //this.mapMode = true;

    /**
     * $onChanges
     */
    this.$onChanges = function(bindings) {
        if(bindings.ngModel && bindings.ngModel.isFirstChange() && bindings.ngModel.currentValue) {
            this.model = angular.extend(this.model, bindings.ngModel.currentValue);
            this.proxyTypesValues = initializeProxyTypesValues(this.model.types);
        }
    };

    this.getMode = function() {
        return this.mode && this.mode == "sparql" ? "sparql" : "graphql";
    };

    // watch deeply query to update
    $scope.$watch(function() { return $ctrl.model }, update, true);

    /**
     * on model update
     */
    function update() {
        $ctrl.error = "";
        $ctrl.loading = true;
        $ctrl.query = null;
        var params = $ctrl.model;

        // build filters
        var filters = [];
        if(params) {

            // types
            if(params.types && params.types.length) {
                if(params.types.length > 1) {
                    filters.push({"rdf_type": {_in: params.types }});
                } else {
                    filters.push({"rdf_type": {_eq: params.types[0] }});
                }
            }

            // ast
            filters = filters.concat([processFilter(params.ast)].filter(function(n){ return n != undefined }));
        }

        // update ng-model
        $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.model));

        // go query
        var query = new Query("poi",{filters : filters});
        var body = [
            "total",
            "query",
            {results:[
                '_uri',
                'rdf_type',
                {'rdfs_label': ['value','lang']},
                {'rdfs_comment': ['value','lang']},
                {'isLocatedAt': [
                    {'schema_address': [
                        {'hasAddressCity': [{
                            'rdfs_label': ['value','lang']
                        }]},
                        'schema_postalCode'
                    ]},
                    {'schema_geo': [
                        'schema_latitude',
                        'schema_longitude'
                    ]}
                ]},
                {'takesPlaceAt': [
                    "startDate",
                    "endDate"
                ]}
            ]}
        ];
        query.find(body);

        if($ctrl.getMode() == "graphql") {
            $ctrl.query = formatGraphQL(filters, body);
        }

        graphqlClient.query(query).then(function(data) {
            $ctrl.resultSet = data.poi;
            if($ctrl.getMode() == "sparql") {
                $ctrl.query = formatSparql(data.poi.query);
            }
            $ctrl.loading = false;
        }, function(error) {
            $ctrl.error = error;
            $ctrl.loading = false;
        });
    }

    /**
     * @param filter
     */
    function processFilter(filter) {
        if(filter) {
            var key = Object.keys(filter)[0];
            var params = filter[key];

            if(key == "or" || key == "and")  {
                var obj = {};
                var op = "_" + key;
                obj[op] = [];
                for(var i=0; i<params.length; i++) {
                    var child = processFilter(params[i]);
                    if(child) {
                        obj[op].push(child);
                    }
                }
                if(obj[op].length > 0) {
                    // ====================================
                    // POST PROCESS : two filter on the same property is transform to _or
                    // ====================================
                    var groups = {};
                    for(var j=0; j<obj[op].length; j++) {
                        var k = Object.keys(obj[op][j])[0];
                        if(k != "_or" && k != "_and") {
                            if(groups[k]) {
                                var k2 = Object.keys(groups[k][k])[0];
                                if(k2 != "_or") {
                                    groups[k][k] = {_or:[angular.copy(groups[k][k])]};
                                }
                                groups[k][k]._or.push(angular.copy(obj[op][j][k]));
                                obj[op].splice(j, 1);
                                j--;
                            } else {
                                groups[k] = obj[op][j];
                            }
                        }
                    }
                    // ====================================
                    if(obj[op].length == 1) {
                        return obj[op][0];
                    }
                    return obj;
                }
            } else {
                var uri = params.path[params.path.length - 1 ];
                var filter = filtersRepository.resolve(uri, key);
                if(filter) {
                    return filter.graphql(params.path, params.value);
                }
            }
        }
        return null;
    }

    /**
     * Trigger when the types selection change
     */
    this.onProxyTypesValuesChange = function() {
        var types = [];
        var typesMap = this.proxyTypesValues.map(function(o) {
            return o.uri;
        });
        for(var i=0; i<this.proxyTypesValues.length; i++) {
            var type = this.proxyTypesValues[i];
            var parent = ontology.getClass(type.parent);
            if(types.indexOf(parent.getUri()) > -1) {
                continue;
            }
            var children = parent.getSubClasses(true);

            var completeGroup = true;
            for(var j=0; j<children.length; j++) {
                if(typesMap.indexOf(children[j].getUri()) < 0) {
                    completeGroup = false;
                    break;
                }
            }

            if(completeGroup) {
                types.push(parent.getUri());
            } else {
                types.push(type.uri);
            }
        }

        this.model.types = types;
    };

    /**
     *
     */
    this.showQueryPreview = function() {
        dialogService.open({
            template: require('./query-preview-modal.html'),
            controller: queryPreviewModalController,
            controllerAs: "$ctrl",
            resolve: {
                mode: function() {
                    return $ctrl.getMode();
                },
                query: function() {
                    return $ctrl.query;
                }
            }
        });
    };

    /**
     *
     * @param types
     * @returns {Array}
     */
    function initializeProxyTypesValues(types) {
        var values = [];
        if(types) {
            for(var i=0; i<$ctrl.typeOptions.length; i++) {
                var opt = $ctrl.typeOptions[i];
                if(types.indexOf(opt.uri) > -1 || types.indexOf(opt.parent) > -1) {
                    values.push(opt);
                }
            }
        }
        return values;
    }
}

/**
 * Fill the typeOptions for types selection
 *
 * @param root
 * @returns {Array}
 */
function getTypeOptions(root) {
    var types = [];
    if(!root) {
        return types;
    }

    var classes = root.getSubClasses(true);

    var icons = {
        "EntertainmentAndEvent": "calendar",
        "PlaceOfInterest": "flag",
        "Product": "euro",
        "Tour": "map-signs"
    };

    for(var i=0; i<classes.length; i++) {
        var subclasses = classes[i].getSubClasses(true);
        var parts = classes[i].getUri().split(/[\/:#]/);
        for(var j=0; j<subclasses.length; j++) {
            types.push({
                uri: subclasses[j].getUri(),
                label: subclasses[j].getPreferedLabel(),
                parent: classes[i].getUri(),
                group: "<i class='fa fa-" + icons[parts[parts.length - 1]] + "'></i>&nbsp;" + classes[i].getPreferedLabel()
            });
        }
    }

    types.sort(function(a,b) {return (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0);} );

    return types;
}

/**
 * Format SPARQL query
 */
function formatSparql(query) {
    var parser = new SparqlParser();
    var generator = new SparqlGenerator();

    var parsedQuery;
    try {
        parsedQuery = parser.parse(query);
    } catch(e) {
        return null;
    }

    // get subquery
    var subSelect = null;
    for(var i=0; i<parsedQuery.where.length; i++) {
        if(parsedQuery.where[i].type == "query") {
            subSelect = parsedQuery.where[i];
        }
    }
    subSelect.limit = null;

    var rx = /WHERE[\r\n\t\f\v ]*\{([^]*)\}[^\}]*$/g;
    var arr = rx.exec(generator.stringify(subSelect));
    if(!arr) {
        return null;
    }

    var where = arr[1];
    // construct final query
    var finalQuery = "CONSTRUCT { \n" +
        "  ?res <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <urn:resource>. \n" +
        "} WHERE { \n" + where.trim() + "\n}";
    return finalQuery;

    /** snippet **/
    /*CONSTRUCT {
        ?s ?p ?o
     } WHERE {
         ?poi a <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest> .
         ?poi <https://www.datatourisme.gouv.fr/resource/metadata#hasFluxIdentifier> "42"^^xsd:int .
         ?poi <https://www.datatourisme.gouv.fr/resource/metadata#hasOrganizationIdentifier> "8"^^xsd:int .
         ?poi (!<>)* ?s.
         ?s ?p ?o.
     }*/
}

/**
 * Format GraphQL query
 */
function formatGraphQL(filters, body) {
    return "query {\n" +
        "   poi("+ (filters.length > 0 ? "filters: [\n" + formatGraphQLFilters(filters, 2) + "   ]" : "") + ") {\n" +
        formatGraphQLBody(body, 2) +
        "   }\n" +
        "}";
}

/**
 * Format GraphQL query
 */
function formatGraphQLFilters(filters, indent) {
    var string = "";
    for(var i=0; i<filters.length; i++) {
        var key = Object.keys(filters[i])[0];
        string += Array(indent+1).join("   ") + "{" + key + ": ";
        var json = JSON.stringify(filters[i][key], null, 3).split("\n");
        for(var j=0; j<json.length; j++) {
            json[j] = json[j].replace(/"([^"]+)":/g, "$1:");
            string += Array(j>0 ? indent+1 : 0).join("   ") + json[j];
            string += j < (json.length -1) ? "\n" : "";
        }
        string += i < (filters.length -1) ? "},\n" :"}\n";
    }
    return string;
}

/**
 * Format GraphQL query
 */
function formatGraphQLBody(body, indent) {
    var string = "";
    for(var i=0; i<body.length; i++) {
        if(typeof body[i] == "string") {
            if(body[i] == "query") continue;
            string += Array(indent+1).join("   ") + body[i] + "\n";
        } else if(typeof body[i] == "object") {
            var key = Object.keys(body[i])[0];
            string += Array(indent+1).join("   ") + key + " {\n";
            string += formatGraphQLBody(body[i][key], indent+1);
            string += Array(indent+1).join("   ") + "}\n";
        }
    }
    return string;
}

/**
 * @ngInject
 */
function queryPreviewModalController(mode, query, $timeout) {
    var $ctrl = this;

    // hack to handle modal timeout
    $timeout(function() {
        $ctrl.ready = true;
    }, 200);

    this.query = query;
    this.options = {
        lineWrapping : true,
        lineNumbers: false,
        readOnly: true,
        //theme:'dracula',
        mode: mode
    };
}