/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .component('filterInputPeriod', {
        controller: controller,
        template: require("./filter-input-period.html"),
        require: {
            ngModelCtrl: 'ngModel'
        }
    });

/**
 * @ngInject
 */
function controller(ngDialog, $http, $timeout, $translate) {
    var $ctrl = this;
    this.period = {startDate: null, endDate: null};

    this.$onInit = function() {
        var ngModel = this.ngModelCtrl;
        ngModel.$render = function() {
            $ctrl.period.startDate = moment(ngModel.$modelValue[0]);
            $ctrl.period.endDate = moment(ngModel.$modelValue[1]);
        };
    };

    this.options = {
        autoApply: true,
        locale: {
            format: $translate.instant("filter.period.date_format"),
            separator: " " + $translate.instant("filter.period.separator") + " ",
            applyLabel: $translate.instant("filter.period.apply"),
            cancelLabel: $translate.instant("label.cancel"),
            fromLabel: $translate.instant("filter.period.from"),
            toLabel: $translate.instant("filter.period.to"),
            //"customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                $translate.instant("filter.period.days.6"),
                $translate.instant("filter.period.days.0"),
                $translate.instant("filter.period.days.1"),
                $translate.instant("filter.period.days.2"),
                $translate.instant("filter.period.days.3"),
                $translate.instant("filter.period.days.4"),
                $translate.instant("filter.period.days.5"),
            ],
            "monthNames": [
                $translate.instant("filter.period.months.0"),
                $translate.instant("filter.period.months.1"),
                $translate.instant("filter.period.months.2"),
                $translate.instant("filter.period.months.3"),
                $translate.instant("filter.period.months.4"),
                $translate.instant("filter.period.months.5"),
                $translate.instant("filter.period.months.6"),
                $translate.instant("filter.period.months.7"),
                $translate.instant("filter.period.months.8"),
                $translate.instant("filter.period.months.9"),
                $translate.instant("filter.period.months.10"),
                $translate.instant("filter.period.months.11"),
            ],
            "firstDay": 1
        },
        eventHandlers: {
            'apply.daterangepicker': function(test) {
                if($ctrl.period.startDate && $ctrl.period.endDate) {
                    var startDate = $ctrl.period.startDate;
                    var endDate = $ctrl.period.endDate;
                    $ctrl.ngModelCtrl.$setViewValue([startDate.format("YYYY-MM-DD"), endDate.format("YYYY-MM-DD")]);
                } else {
                    $ctrl.ngModelCtrl.$setViewValue(null);
                }
            }
        }
    };
}
