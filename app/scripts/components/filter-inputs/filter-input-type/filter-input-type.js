/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .component('filterInputType', {
        controller: controller,
        template: require("./filter-input-type.html"),
        require: {
            ngModelCtrl: 'ngModel'
        },
        bindings: {
            ngModel: '=',
            property: '<',
            types: '<'
        }
    });

/**
 * @ngInject
 */
function controller(ontology, $translate) {
    var $ctrl = this;

    this.options = [];
    this.config = {
        //optgroupField: 'group',
        //optgroupValueField: 'uri',
        //optgroupLabelField: 'label',
        searchField: ['label'],
        valueField: 'uri',
        labelField: 'label',
        placeholder: $translate.instant('placeholder.types'),
        //optgroups: []
    };

    this.$onChanges = function (obj) {

        var superClasses = [ontology.NS + "PointOfInterest"];
        if(obj.types && obj.types.currentValue && obj.types.currentValue.length  > 0) {
            superClasses = obj.types.currentValue;
        }

        var classes = [];
        for(var i=0; i<superClasses.length; i++) {
            var ontClass = ontology.getClass(superClasses[i]);

            if (ontClass) {
                var subClasses = ontClass.getSubClasses(false);
                for(var j=0; j<subClasses.length; j++) {
                    if(classes.indexOf(subClasses[j]) < 0) {
                        classes.push(subClasses[j]);
                    }
                }
            }
        }

        var options = [];
        for(var i=0; i<classes.length; i++) {
            var ontClass = classes[i];
            options.push({
                uri: ontClass.getUri(),
                label: ontClass.getPreferedLabel()
            });
        }

        options = options.sort(sortByLabel);
        this.options = options;
    };

}

/**
 * @param a
 * @param b
 * @returns {number}
 */
function sortByLabel(a, b) {
    if(a.label < b.label) return -1;
    else if(b.label < a.label) return 1;
    return 0;
}
