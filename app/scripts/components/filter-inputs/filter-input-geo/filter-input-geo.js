/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .component('filterInputGeo', {
        controller: controller,
        template: require("./filter-input-geo.html"),
        require: {
            ngModelCtrl: 'ngModel'
        }
    });

/**
 * @ngInject
 */
function controller(dialogService, $http, $timeout) {
    var $ctrl = this;

    /**
     * Format model
     *
     * @returns {*}
     */
    this.formatModel = function() {
        if(this.ngModelCtrl.$modelValue) {
            var value = this.ngModelCtrl.$modelValue;
            return "["+ (Math.round(value[0][0]*10000)/10000) +", "+ (Math.round(value[0][1]*10000)/10000) +"] rayon "+ value[1] +"km";
        }
        return "";
    };

    /**
     * Edit the filter
     */
    this.edit = function() {
        var value = $ctrl.ngModelCtrl.$modelValue;
        var dialog = dialogService.open({
            template: require('./filter-input-geo-modal.html'),
            closeByDocument: true,
            closeByEscape: true,
            controller: modalController,
            resolve: {
                center: function() {
                    if(value) {
                        return value[0];
                    } else {
                        return $http({url: "http://freegeoip.net/json/"}).then(function(res) {
                            return [res.data.latitude, res.data.longitude];
                        }, function() {
                            return [48.1119800, -1.6742900];
                        });
                    }
                },
                radius: function() {
                    return value ? value[1] : 10;
                }
            },
            controllerAs: "$ctrl"
        });

        dialog.then(function(value) {
            $ctrl.ngModelCtrl.$setViewValue(value);
        }, function() {});
    };

}

/**
 * @ngInject
 */
function modalController(leafletData, center, radius, $scope, $timeout) {
    var $ctrl = this;
    var circle = null;
    var map = null;

    // hack to handle modal timeout
    $timeout(function() {
       $ctrl.ready = true;
    }, 200);

    this.center = {
        lat: center[0],
        lng: center[1],
        zoom: 10
    };

    this.radius = radius;

    this.onSlide = function(e, value) {
        //$(".radius-helper").html(e.value);
        circle.setRadius(value * 1000);
    };

    this.onStopSlide = function(e, value) {
        map.fitBounds(circle.getBounds());
    };

    leafletData.getMap("filter-geo-map").then(function(_map) {
        map = _map;
        circle = L.circle(map.getCenter());
        circle.setRadius(radius * 1000);
        circle.addTo(map);
        map.on('move', function (e) {
            circle.setLatLng(map.getCenter());
            //$ctrl.center = map.getCenter();
        });
        map.fitBounds(circle.getBounds());
    });

    this.confirm = function() {
        $scope.closeThisDialog([[this.center.lat, this.center.lng], this.radius]);
    }
}