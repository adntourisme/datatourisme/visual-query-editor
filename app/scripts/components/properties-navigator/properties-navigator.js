/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .component('propertiesNavigator', {
        template: require("./properties-navigator.html"),
        controller: controller,
        require: {
            ngModelCtrl: 'ngModel'
        }
    });

/**
 * @ngInject
 */
function controller(ontology, filtersRepository, $timeout) {
    var $ctrl = this;

    var root = ontology.getClass(ontology.NS + "PointOfInterest");
    this.steps = [buildStep([root])];

    /**
     * @param property
     */
    this.selectProperty = function(property) {
        if(this.isCompound(property)) {
            this.ngModelCtrl.$setViewValue(null);
            var step = buildStep(property);
            this.steps.push(step);
            $timeout(fixWrapperOffset, 100);
        } else {
            var filter = filtersRepository.resolveDefinition(property.uri);
            this.ngModelCtrl.$setViewValue({
                type: filter.type,
                path: getCurrentPath(property)
            });
        }
    };

    /**
     * Return true if property is compound
     *
     * @param property
     * @returns {boolean}
     */
    this.isCompound = function(property) {
        return (property.getType() == "object" || filtersRepository.getDefinitions(property.uri).length > 1);
    };

    /**
     * Check if property is currently active
     *
     * @param property
     * @returns {boolean}
     */
    this.isActiveProperty = function(property) {
        if($ctrl.ngModelCtrl.$modelValue) {
            return angular.equals($ctrl.ngModelCtrl.$modelValue.path, getCurrentPath(property));
        }
        return false;
    };

    /**
     * @param filter
     */
    this.selectFilter = function(filter) {
        this.ngModelCtrl.$setViewValue({
            type: filter.type,
            path: getCurrentPath()
        });
    };

    /**
     * Check if filter is currently active
     *
     * @param filter
     * @returns {boolean}
     */
    this.isActiveFilter = function(filter) {
        if($ctrl.ngModelCtrl.$modelValue) {
            return angular.equals($ctrl.ngModelCtrl.$modelValue, {
                type: filter.type,
                path: getCurrentPath()
            });
        }
        return false;
    };

    /**
     * @param property
     */
    this.goToPreviousStep = function(step) {
        if(step) {
            var index = this.steps.indexOf(step);
            this.steps = this.steps.slice(0, index+1);
        } else {
            this.steps.pop();
        }
        $timeout(fixWrapperOffset, 100);
    };

    /**
     * @param mainClass
     */
    function buildStep(arg) {
        var classes, candidateProperties, properties, i, j;
        var step = {
            property: null,
            classes: [],
            properties: [],
            filters: []
        };

        if(arg instanceof Array) {
            classes = arg;
        } else {
            step.property = arg;
            classes = arg.getRanges();
        }

        properties = [];

        for(i=0; i<classes.length; i++) {
            // get class transversal properties
            candidateProperties = classes[i].getCandidateProperties();
            if(!candidateProperties || !candidateProperties.length) {
                continue;
            }

            for(j=0; j<candidateProperties.length; j++) {
                if(properties.indexOf(candidateProperties[j]) < 0) {
                    properties.push(candidateProperties[j]);
                }
            }

            step.classes.push(classes[i]);
        }

        step.properties = properties;

        // additionals filters
        if(step.property) {
            var filters = filtersRepository.getDefinitions(step.property.uri);
            for(i=0; i<filters.length; i++) {
                step.filters.push(filters[i]);
            }
        }

        return step;
    }

    /**
     * Return the current path for the given property
     */
    function getCurrentPath(property) {
        var parts = [];
        for(var i=0; i<$ctrl.steps.length; i++) {
            var step = $ctrl.steps[i];
            if(step.property) {
                parts.push(step.property.getUri());
            }
        }
        if(property) {
            parts.push(property.getUri());
        }
        return parts;
    }

    /**
     * Fix wrapper offset
     */
    function fixWrapperOffset() {
        var offset = 0;
        if($ctrl.steps.length > 0) {
            var width = $(".steps-container > div").eq(0).outerWidth();
            offset = width * ($ctrl.steps.length - 1);
        }
        $ctrl.offset = offset;
    }
}
