/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var gql = require('graphql-tag');
var apollo = require('apollo-client');
var Query = require('graphql-query-builder');

require('angular')
    .module('visual-query-editor')
    .provider('filtersRepository', filtersRepositoryProvider)
    .config(config)

/**
 * @ngInject
 */
function filtersRepositoryProvider() {
    var _default_ = null;
    var filters = [];

    this.setDefault = function(filterDef) {
        _default_ = filterDef;
    };

    this.add = function(filterDef) {
        filters.push(filterDef);
    };

    /**
     * @ngInject
     */
    this.$get = function(ontology) {

        return {
            getDefinitions: getDefinitions,
            resolve: resolve,
            resolveDefinition: resolveDefinition
        };

        /**
         * Get all supporting filters with property uri
         */
        function getDefinitions(uri) {
            var _filters = [];
            var property = ontology.getProperty(uri);
            var ranges = property.getRanges();
            for(var i=0; i<ranges.length; i++) {
                for(var j=0; j<filters.length; j++) {
                    if(filters[j].supports(ranges[i], property)) {
                        if(_filters.indexOf(filters[j]) < 0) {
                            _filters.push(filters[j]);
                        }
                    }
                }
            }
            return _filters;
        }

        /**
         * Resolve a filter with property uri and type
         */
        function resolveDefinition(uri, type) {
            var filters = getDefinitions(uri);
            for(var i=0; i<filters.length; i++) {
                if(!type || filters[i].type == type) {
                    return filters[i];
                }
            }
            if(filters.length) {
                return filters[0];
            }
            return _default_;
        }

        /**
         * Resolve a filter with property uri and type
         */
        function resolve(uri, type) {
            var definition = resolveDefinition(uri, type);
            var property = ontology.getProperty(uri);
            if(definition && property) {
                return new definition(property);
            }
            return null;
        }

    };
}

/**
 * @ngInject
 */
function config(filtersRepositoryProvider) {
    // default : text filter
    filtersRepositoryProvider.setDefault(require("./filters/text-filter"));
    // add others
    filtersRepositoryProvider.add(require("./filters/number-filter"));
    filtersRepositoryProvider.add(require("./filters/bool-filter"));
    filtersRepositoryProvider.add(require("./filters/geo-filter"));
    filtersRepositoryProvider.add(require("./filters/thesaurus-filter"));
    filtersRepositoryProvider.add(require("./filters/autocomplete-filter"));
    filtersRepositoryProvider.add(require("./filters/period-filter"));
    filtersRepositoryProvider.add(require("./filters/uri-filter"));
    filtersRepositoryProvider.add(require("./filters/now-filter"));
    filtersRepositoryProvider.add(require("./filters/type-filter"));
    filtersRepositoryProvider.add(require("./filters/time-filter"));
}