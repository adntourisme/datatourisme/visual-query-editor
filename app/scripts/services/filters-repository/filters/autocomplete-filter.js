/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'autocomplete';

Filter.definition = {
    label: "filter.autocomplete.label",
    helper: "filter.autocomplete.helper",
    comment: "filter.autocomplete.comment"
};

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res) {
    return false;
    var properties = res.getCandidateProperties();
    for(var j=0; j<properties.length; j++) {
        if(properties[j].hasUri("http://www.w3.org/2000/01/rdf-schema#label")) {
            return true;
        }
    }
    return false;
};


/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<div>todo</div>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    return this.graphqlPath(path, {_geo_distance: {lat: value[0][0], lng:value[0][1], radius: value[1]} });
};


module.exports = Filter;