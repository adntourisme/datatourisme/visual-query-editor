/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'thesaurus';

Filter.definition = {
    label: "filter.thesaurus.label",
    helper: "filter.thesaurus.helper",
    comment: "filter.thesaurus.comment"
};

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res) {
    return res.isThesaurus();
};

/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<filter-input-thesaurus ng-model="$ctrl.ngModel" property="$ctrl.property"></filter-input-thesaurus>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    if(value.length > 1) {
        return this.graphqlPath(path, {_in: value });
    } else {
        return this.graphqlPath(path, {_eq: value[0] });
    }
};


module.exports = Filter;