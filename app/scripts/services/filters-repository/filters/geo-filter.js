/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'geo';

Filter.definition = {
    label: "filter.geo.label",
    helper: "filter.geo.helper",
    comment: "filter.geo.comment"
};

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res) {
    return res.hasUri("http://schema.org/GeoCoordinates");
};


/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<filter-input-geo ng-model="$ctrl.ngModel"></filter-input-geo>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    return this.graphqlPath(path, {_geo_distance: {lat: value[0][0], lng:value[0][1], distance: value[1]} });
};


module.exports = Filter;