/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'bool';

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res) {
    return res.hasUri("http://www.w3.org/2001/XMLSchema#bool") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#boolean");
};

/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<select class="form-control input-sm" ng-model="$ctrl.ngModel" ng-options="o.v as (o.n | translate) for o in [{ n:  \'label.yes\', v: true }, { n: \'label.no\', v: false }]" required></select>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    return this.graphqlPath(path, {_eq: value });
};


module.exports = Filter;