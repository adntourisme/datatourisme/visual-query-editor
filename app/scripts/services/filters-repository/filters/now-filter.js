/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'text';

Filter.definition = {
    label: "filter.now.label",
    helper: "filter.now.helper",
    comment: "filter.now.comment"
};

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res) {
    return res.hasUri("http://www.w3.org/2001/XMLSchema#date") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#dateTime");
};

/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<select class="form-control input-sm" ng-model="$ctrl.ngModel" required>' +
        '<option value="_gt">{{ "label.forthcoming" | translate }}</option>' +
        '<option value="_lt">{{ "label.past" | translate }}</option>' +
    '</select>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    var obj = {};
    obj[value] = "_now_";
    return this.graphqlPath(path, obj);
};


module.exports = Filter;