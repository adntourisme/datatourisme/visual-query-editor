/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var angular = require("angular");

var Filter = function (property) {
    this.property = property;
    this.ontology = property.getOntology();

    /**
     * Transform a path array to a GraphQL path
     *
     * @param path
     * @param obj
     * @returns {{}}
     */
    this.graphqlPath = function(path, obj) {
        var newObj = {};
        var _obj = newObj;

        for(var i=0; i<path.length; i++) {
            var uri = path[i];
            var key = this.ontology.shortForm(uri).replace(":", "_");
            if(key[0] == "_") { key = key.substr(1); }

            _obj[key] = {};
            _obj = _obj[key];
        }

        angular.extend(_obj, obj);
        return newObj;
    }
};

module.exports = Filter;