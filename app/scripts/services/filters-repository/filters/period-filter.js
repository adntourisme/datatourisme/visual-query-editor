/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'period';

Filter.definition = {
    label: "filter.period.label",
    helper: "filter.period.helper",
    comment: "filter.period.comment"
};

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res) {
    return res.hasUri("http://www.w3.org/2001/XMLSchema#date") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#dateTime");
};


/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<filter-input-period ng-model="$ctrl.ngModel"></filter-input-period>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    return this.graphqlPath(path, {_and: [{_gte: value[0]}, {_lte:value[1]}]});
};


module.exports = Filter;