/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'type';

Filter.definition = {
    label: "filter.type.label",
    helper: "filter.type.helper",
    comment: "filter.type.comment"
};

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(range, property) {
    return range.hasUri('https://www.datatourisme.gouv.fr/ontology/core#PointOfInterestClass');
    //return property.hasUri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
};

/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<filter-input-type ng-model="$ctrl.ngModel" property="$ctrl.property" types="$ctrl.types"></filter-input-type>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    if(value.length > 1) {
        return this.graphqlPath(path, {_in: value });
    } else {
        return this.graphqlPath(path, {_eq: value[0] });
    }
};


module.exports = Filter;