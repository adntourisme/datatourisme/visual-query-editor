/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'autocomplete';

Filter.definition = {
    label: "filter.uri.label",
    helper: "filter.uri.helper",
    comment: "filter.uri.comment"
};

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res, property) {
    return property.getType() == 'object'
        && !res.isThesaurus();
};


/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<input type="text" class="form-control input-sm" ng-model="$ctrl.ngModel" ng-model-options="{ allowInvalid: false, debounce: 500 }" required empty-to-null>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    return this.graphqlPath(path, {_eq: value });
};


module.exports = Filter;