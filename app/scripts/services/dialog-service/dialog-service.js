/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

// service definition
require('angular')
    .module('visual-query-editor')
    .factory('dialogService', dialogService);

/**
 * @ngInject
 */
function dialogService(ngDialog, bsNgDialog, $q) {

    var service = {
        open: open
    };

    return service;

    /**
     * Open
     */
    function open(params) {
        var dialog;
        if($(".vqe-offcanvas-container").length > 0) {
            dialog = ngDialog.open(angular.extend({}, {
                appendTo: '.vqe-offcanvas-container',
                className: 'vqe-offcanvas',
                bodyClassName: 'vqe-offcanvas-open',
                plain: true,
                trapFocus: false,
                showClose: false
            }, params));
        } else {
            params.template = wrapBsModalTemplate(params.template);
            dialog = bsNgDialog.open(angular.extend({}, {
                plain: true,
                className: 'vqe-modal'
            }, params));
        }

        var deferred = $q.defer();
        dialog.closePromise.then(function(dialog) {
            if(dialog.value && (typeof dialog.value !== "string" || dialog.value.indexOf("$") !== 0)) {
                deferred.resolve(dialog.value);
            } else {
                deferred.reject(dialog.value);
            }
        });
        return deferred.promise;
    }

    /**
     * @param template
     * @returns {string}
     */
    function wrapBsModalTemplate(template) {
        return '<div class="modal fade" tabindex="-1" role="dialog">' +
            '<div class="modal-dialog modal-lg" role="document">' +
            '<div class="modal-content">' + template + '</div></div></div>'
    }
}