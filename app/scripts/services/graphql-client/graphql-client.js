/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var gql = require('graphql-tag');
var apollo = require('apollo-client');
var Query = require('graphql-query-builder');

require('angular')
    .module('visual-query-editor')
    .provider('graphqlClient', graphqlClientProvider);

/**
 * @ngInject
 */
function graphqlClientProvider() {
    var endpoint = 'http://localhost:8080';

    this.setEndpoint = function(value) {
        endpoint = value;
    };

    /**
     * @ngInject
     */
    this.$get = function($q) {

        var networkInterface = apollo.createNetworkInterface({
            uri: endpoint,
            opts: {
                credentials: 'same-origin'
            }
        });
        var client = new apollo.ApolloClient({
            networkInterface: networkInterface
        });

        return {
            query: query
        };

        /**
         * Query the GraphQL endpoint
         */
        function query(query) {
            if(query instanceof Query) {
                query = "{" + query + "}";
            }
            //console.log(query);
            var opts = {query: gql(query) };
            return wrap(client.query(opts)).then(function(data) {
                return data.data;
            });
        }

        /**
         * Wrap an apollo promise
         *
         * @param promise
         * @returns {*}
         */
        function wrap(promise) {
            return $q(function(resolve, reject) {
                return promise.then(resolve).catch(reject);
            });
        }
    };
}


